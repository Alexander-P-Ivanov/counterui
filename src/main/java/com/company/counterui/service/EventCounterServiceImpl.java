package com.company.counterui.service;

import com.company.counterui.LastPeriod;
import com.company.counterui.dao.EventCounterDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;

@Component
public class EventCounterServiceImpl implements EventCounterService {

    @Autowired
    EventCounterDao eventCounterDao;


    @Override
    public void saveEvent(String user) {
        eventCounterDao.saveEvent(new Timestamp(new Date().getTime()), user);
    }

    @Override
    public BigInteger getEventCountForPeriod(LastPeriod forPeriod) {
        Date currentDate = new Date();

        Calendar calendar = Calendar.getInstance();

        calendar.setTime(currentDate);

        switch (forPeriod) {
            case MINUTE:
                calendar.add(Calendar.MINUTE, -1);
                break;
            case HOUR:
                calendar.add(Calendar.HOUR, -1);
                break;
            case DAY:
                calendar.add(Calendar.DATE, -1);
                break;
        }

        return eventCounterDao.getEventCountAfterTimestamp(new Timestamp(calendar.getTimeInMillis()));
    }

    @Override
    public EventCounterDao getDao() {
        return eventCounterDao;
    }

    @Override
    public void setDao(EventCounterDao dao) {
        this.eventCounterDao = dao;
    }
}
