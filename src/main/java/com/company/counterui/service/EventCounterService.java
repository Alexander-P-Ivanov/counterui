package com.company.counterui.service;

import com.company.counterui.LastPeriod;
import com.company.counterui.dao.EventCounterDao;
import org.springframework.stereotype.Service;

import java.math.BigInteger;

@Service
public interface EventCounterService {
    void saveEvent(String user);
    
    BigInteger getEventCountForPeriod(LastPeriod forPeriod);

    EventCounterDao getDao();

    void setDao(EventCounterDao dao);
}
