package com.company.counterui;

import com.company.counterui.service.EventCounterService;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.Button;
import com.vaadin.ui.Notification;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigInteger;

@SpringUI
public class CounterUI extends UI {
    
    @Autowired
    EventCounterService eventCounterService;
    
    @Override
    protected void init(VaadinRequest vaadinRequest) {
        VerticalLayout layout = new VerticalLayout();
        
        Button countEventButton = new Button("Учесть событие");
        Button eventsPerMinuteButton = new Button("Событий за минуту");
        Button eventsPerHourButton = new Button("Событий за час");
        Button eventsPerDayButton = new Button("Событий за 24 часа");
        
        
        layout.addComponents(countEventButton, eventsPerMinuteButton, eventsPerHourButton, eventsPerDayButton);
        layout.setSpacing(true);
        layout.setMargin(true);
        
        countEventButton.addClickListener(clickEvent -> {
            eventCounterService.saveEvent("user_546");
            Notification.show("Событие");
        });
        
        eventsPerMinuteButton.addClickListener(clickEvent -> showCount(LastPeriod.MINUTE));
        eventsPerHourButton.addClickListener(clickEvent -> showCount(LastPeriod.HOUR));
        eventsPerDayButton.addClickListener(clickEvent -> showCount(LastPeriod.DAY));
        
        setContent(layout);
    }
    
    private void showCount(LastPeriod lastPeriod) {
        String periodName;
        
        switch (lastPeriod) {
            case MINUTE:
                periodName = "минуту";
                break;
            case HOUR:
                periodName = "час";
                break;
            case DAY:
                periodName = "24 часа";
                break;
            default:
                periodName = "";
                break;
        }
    
    
        BigInteger eventCountForPeriod = eventCounterService.getEventCountForPeriod(lastPeriod);
        
        String resultCaption = "Событий за " + periodName + ": " + eventCountForPeriod;
        
        Notification.show(resultCaption);
    }
}
