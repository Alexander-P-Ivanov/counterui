package com.company.counterui;

public enum LastPeriod {
    MINUTE, HOUR, DAY
}
