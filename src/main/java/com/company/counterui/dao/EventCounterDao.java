package com.company.counterui.dao;

import java.math.BigInteger;
import java.sql.Timestamp;

public interface EventCounterDao {
    void saveEvent(Timestamp ts, String name);
    
    BigInteger getEventCountAfterTimestamp(Timestamp timestamp);
}
