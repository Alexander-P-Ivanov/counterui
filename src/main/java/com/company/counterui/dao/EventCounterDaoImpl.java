package com.company.counterui.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.sql.Timestamp;

@Transactional
@Component
public class EventCounterDaoImpl implements EventCounterDao {
    
    @Autowired
    JdbcTemplate jdbcTemplate;
    
    @Override
    public void saveEvent(Timestamp ts, String name) {
        jdbcTemplate.update("INSERT INTO events(date, user) VALUES(?, ?)", ts, name);
    }
    
    @Override
    public BigInteger getEventCountAfterTimestamp(Timestamp timestamp) {
        return jdbcTemplate.queryForObject("SELECT COUNT(*) FROM events WHERE date>?", BigInteger.class, timestamp);
        
    }
}
