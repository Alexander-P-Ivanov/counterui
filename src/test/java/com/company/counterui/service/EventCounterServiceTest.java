package com.company.counterui.service;

import com.company.counterui.LastPeriod;
import com.company.counterui.dao.EventCounterDao;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import java.math.BigInteger;
import java.util.Random;

public class EventCounterServiceTest {
    public static final String MOCKED_COUNT = "714";

    private static EventCounterDao eventCounterDao;

    private static EventCounterService eventCounterService;

    @BeforeClass
    public static void beforeClass() {
        eventCounterService = new EventCounterServiceImpl();

        eventCounterDao = Mockito.mock(EventCounterDao.class);

        eventCounterService.setDao(eventCounterDao);

        Mockito.when(eventCounterDao.getEventCountAfterTimestamp(Mockito.any())).thenReturn(new BigInteger(MOCKED_COUNT));

        Mockito.doNothing().when(eventCounterDao).saveEvent(Mockito.any(), Mockito.any());

    }

    @Test
    public void testGetEventCountForPeriod() {
        LastPeriod lastPeriod = LastPeriod.values()[new Random().nextInt(LastPeriod.values().length)];

        BigInteger countForPeriod = eventCounterService.getEventCountForPeriod(lastPeriod);

        Mockito.verify(eventCounterDao).getEventCountAfterTimestamp(Mockito.any());

        Assert.assertEquals(new BigInteger(MOCKED_COUNT), countForPeriod);
    }

    @Test
    public void testSaveEvent() {
        eventCounterService.saveEvent("user_546");

        Mockito.verify(eventCounterDao).saveEvent(Mockito.any(), Mockito.any());
    }
}
