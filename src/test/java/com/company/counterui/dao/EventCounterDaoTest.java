package com.company.counterui.dao;

import com.company.counterui.config.AppTestConfiguration;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = AppTestConfiguration.class)
public class EventCounterDaoTest extends DBUnitSetup {

    @Autowired
    private EventCounterDao eventCounterDao;

    @Test
    public void testGetEventCountAfterTimestamp() throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Date date = simpleDateFormat.parse("2017-07-09 00:00:00");

        BigInteger eventCountFromTimestamp = eventCounterDao.getEventCountAfterTimestamp(new Timestamp(date.getTime()));

        Assert.assertEquals(new BigInteger("3"), eventCountFromTimestamp);
    }


    @Test
    public void testSaveEvent() throws ParseException {
        final Timestamp ts = new Timestamp(new Date().getTime());

        eventCounterDao.saveEvent(ts, "user_546");

        ts.setTime(ts.getTime() - 1);

        final BigInteger eventCountAfterTimestamp = eventCounterDao.getEventCountAfterTimestamp(ts);

        Assert.assertEquals(new BigInteger("1"), eventCountAfterTimestamp);
    }
}
