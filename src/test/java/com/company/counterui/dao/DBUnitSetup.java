package com.company.counterui.dao;

import org.dbunit.DatabaseUnitException;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.ext.hsqldb.HsqldbDataTypeFactory;
import org.dbunit.operation.DatabaseOperation;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.io.File;
import java.net.MalformedURLException;
import java.sql.SQLException;

public class DBUnitSetup {
    private static IDatabaseConnection connection;
    private static IDataSet dataSet;
    
    @Autowired
    DataSource dataSource;
    
    @PostConstruct
    public void init() throws SQLException, DatabaseUnitException, MalformedURLException {
        connection = new DatabaseConnection(dataSource.getConnection());
        connection.getConfig().setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new HsqldbDataTypeFactory());
    
        FlatXmlDataSetBuilder flatXmlDataSetBuilder = new FlatXmlDataSetBuilder();
    
        dataSet = flatXmlDataSetBuilder.build(new File("test-data.xml"));
    
        DatabaseOperation.CLEAN_INSERT.execute(connection, dataSet);
    }
}
